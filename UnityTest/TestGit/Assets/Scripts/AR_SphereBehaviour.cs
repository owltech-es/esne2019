﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AR_SphereBehaviour : MonoBehaviour
{
    [SerializeField] float value = 0f;
    [SerializeField] float speed = 01f;


    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0, Mathf.Sin(value), 0);
        value += speed;
    }
}
